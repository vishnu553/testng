package testng.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Browser {
    WebDriver driver=null;

    public WebDriver browserLaunch(String browserName)
    {
        if(browserName.equalsIgnoreCase("chrome"))
        {
            System.setProperty("webdriver.chrome.driver","divers/chromedriver.exe");
            driver=new ChromeDriver();


        }
       if(browserName.equalsIgnoreCase("mozilla"))
        {
            System.setProperty("webdriver.gecko.driver","divers/geckodriver.exe");
            driver=new FirefoxDriver();
        }
        return  driver;

    }
}
