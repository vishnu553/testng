package testng;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import testng.browser.Browser;
import testng.pages.*;

public class TestPractice {
    WebDriver driver = null;
    String url="https://the-internet.herokuapp.com";
    @BeforeTest
    public void urlHome()throws Exception {
        Browser browser = new Browser();
        driver = browser.browserLaunch("mozilla");

    }
    @BeforeMethod
    public void launchHomePage()
    {
        driver.get(url);
    }
    @AfterTest
    public void quitBrowser()
    {
        driver.close();
    }

    public void test001()throws Exception {
            LoginPage loginPage = new LoginPage(driver);
            HomePage homePage = new HomePage(driver);
            homePage.loginLink();
            loginPage.enterUserName("tomsmith");
            loginPage.enterPassword("SuperSecretPassword!");
            loginPage.clickLogin();
            LogoutPage logoutPage = new LogoutPage(driver);
            String sMsg = logoutPage.checkSecureMsg();
            Assert.assertEquals(sMsg, "Secure Area", "FAil");
            String wlMsg = logoutPage.checkWelcomeMsg();
            Assert.assertEquals(wlMsg, "Welcome to the Secure Area. When you are done click logout below.", "FAIL");
            logoutPage = new LogoutPage(driver);
            logoutPage.clickLogout();

    }
   @Test(priority = 1)
  //@Test(enabled = false)
    public void test002() throws Exception {
        System.out.println("test 2");
        HomePage homePage = new HomePage(driver);
        String ver = homePage.basicAuthLink();
        Assert.assertEquals(ver, "Basic Auth", "FAIL");

    }
}
