package testng;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import testng.browser.Browser;
import testng.pages.HomePage;
import testng.pages.RedirectPage;
import testng.pages.StatusCode;

public class TestPractic {

    WebDriver driver = null;
    String url="https://the-internet.herokuapp.com";
    @BeforeTest
    public void urlHome()throws Exception {
        Browser browser = new Browser();
        driver = browser.browserLaunch("mozilla");


    }
    @BeforeMethod
    public void launchHome()
    {
        driver.get(url);
    }
    @Test
    public void test003()  {

        HomePage homePage = new HomePage(driver);
        homePage.redirectLink();
        RedirectPage redirectPage = new RedirectPage(driver);
        redirectPage.clickHere();
        StatusCode statusCode = new StatusCode(driver);
        String s = statusCode.statusCodeComparison();
        Assert.assertEquals(s, "Status Codes", "FAIL");

    }
    @AfterTest
    public void quitUrl()
    {
        driver.close();
    }
}
