package testng.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
public class HomePage {
    WebDriver driver=null;
    By DIRRECTLINKLOC=By.cssSelector("a[href='/redirector']");
    By LOGINLINKLOC=By.cssSelector("a[href='/login']");

    By VERTEXTLOC=By.cssSelector(".example>h3");
    public HomePage(WebDriver driver)
    {
        this.driver=driver;
    }
    public void loginLink()
    {
        driver.findElement(LOGINLINKLOC).click();
    }
    public void redirectLink()
    {
        driver.findElement(DIRRECTLINKLOC).click();
    }
    public String basicAuthLink()
    {
        driver.get("https://admin:admin@the-internet.herokuapp.com/basic_auth");
        String verText=driver.findElement(VERTEXTLOC).getText();
        return  verText;

    }

}
