package testng.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LogoutPage {
    WebDriver driver;
    By secureMsg=By.cssSelector("#content>div>h2");
    By welcomeMsg=By.cssSelector(" .subheader");
    public LogoutPage(WebDriver driver)
    {
        this.driver=driver;
    }
    By logout=By.cssSelector(".button");

    public void  clickLogout()
    {
        driver.findElement(logout).click();
    }
    public String checkSecureMsg()
    {

        String  getSecureMsg=driver.findElement(secureMsg).getText();
        return  getSecureMsg;
    }
    public String checkWelcomeMsg()
    {
        String getWelcomeMsg=driver.findElement(welcomeMsg).getText();
        return  getWelcomeMsg;
    }

}
