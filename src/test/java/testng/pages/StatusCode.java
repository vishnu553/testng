package testng.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class StatusCode {

    WebDriver driver=null;
    By STATUSCODELOCATOR=By.cssSelector(".example>h3");
    public StatusCode(WebDriver driver)
    {
        this.driver=driver;
    }
    public String statusCodeComparison()
    {
     String getText=driver.findElement(STATUSCODELOCATOR).getText();
     return  getText;

    }
}
