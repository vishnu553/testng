package testng.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    WebDriver webdriver;

    By userName = By.cssSelector("#username");
    By password = By.cssSelector("#password");
    By login = By.cssSelector(".radius");

    public LoginPage(WebDriver webdriver)
    {        this.webdriver=webdriver;
    }
    public void enterUserName(String getUserName)
    {
        webdriver.findElement(userName).sendKeys(getUserName);
    }
    public void enterPassword(String getPassword)
    {
        webdriver.findElement(password).sendKeys(getPassword);
    }
    public void clickLogin()
    {
        webdriver.findElement(login).click();
    }
   /* public void clickUrl()
    {
        webdriver.findElement(CLICKURLLOC).click();

    }
    */


}
